describe "POST /sessions" do
  context "login com sucesso" do
    before(:all) do
      payload = { email: "betao@hotmail.com", password: "pwd123" }

      @result = Sessions.new.login(payload)
      # puts result.parsed_response
      # puts result.parsed_response.class
    end

    it "valida status code" do
      expect(@result.code).to eql 200
    end

    it "valida id do usuário" do
      expect(@result.parsed_response["_id"].length).to eql 24
    end
  end

  # examples = [
  #   { title: "senha incorreta",
  #     payload: { email: "betao@yahoo.com", password: "pwd123123" },
  #     code: 401,
  #     error: "Unauthorized" },
  #   { title: "usuario não existe",
  #     payload: { email: "404@yahoo.com", password: "pwd123123" },
  #     code: 401,
  #     error: "Unauthorized" },
  #   { title: "email em branco",
  #     payload: { email: "", password: "pwd123123" },
  #     code: 412,
  #     error: "required email" },
  #   { title: "sem o campo email",
  #     payload: { password: "pwd123123" },
  #     code: 412,
  #     error: "required email" },
  #   { title: "senha em branco",
  #     payload: { email: "betao@yahoo.com", password: "" },
  #     code: 412,
  #     error: "required password" },
  #   { title: "sem o campo senha",
  #     payload: { email: "betao@yahoo.com" },
  #     code: 412,
  #     error: "required password" },
  # ]

  examples = Helpers::get_fixture("login")

  examples.each do |e|
    context "#{e[:title]}" do
      before(:all) do
        @result = Sessions.new.login(e[:payload])
        # puts result.parsed_response
        # puts result.parsed_response.class
      end

      it "valida status code #{e[:code]}" do
        expect(@result.code).to eql e[:code]
      end

      it "valida msg erro" do
        expect(@result.parsed_response["error"]).to eql e[:error]
      end
    end
  end
end
