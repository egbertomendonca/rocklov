class EquiposPage
  include Capybara::DSL

  def create(equipo)
    # expect(page).to have_css "#equipoForm" - não posso colocar expect em pageObject

    # checkpoint com timeout explicito
    page.has_css?("#equipoForm")

    upload(equipo[:thumb]) if equipo[:thumb].length > 0

    find("input[placeholder$='equipamento']").set equipo[:nome] #^começa, * contem, $ termina
    select_cat(equipo[:categoria]) if equipo[:categoria].length > 0
    find("input[placeholder^=Valor]").set equipo[:preco]

    click_button "Cadastrar"
  end

  def upload(file_name)
    thumb = Dir.pwd + "/features/support/fixtures/images/" + file_name

    find("#thumbnail input[type=file]", visible: false).set thumb
  end

  def select_cat(cat)
    find("#category").find("option", text: cat).select_option
  end
end
